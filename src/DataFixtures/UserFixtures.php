<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use FOS\UserBundle\Model\UserManagerInterface;

class UserFixtures extends Fixture
{
    public $userManager;
    public $faker;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
        $this->faker = Factory::create();
    }
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $testPassword = '123321q';

        $user1 = new User();
        $user1->setUsername($this->faker->firstName);
        $user1->setUsernameCanonical($this->faker->lastName);
        $user1->setPlainPassword($testPassword);
        $user1->setEmail("admin1@example.com");
        $user1->setRoles(['ROLE_ADMIN']);
        $user1->setEnabled(true);
        $user1->setExtraFields($this->getReference('admin-one'));
        $this->addReference('admi1', $user1);
        $this->userManager->updateUser($user1);

        $user2 = new User();
        $user2->setUsername($this->faker->firstName);
        $user2->setUsernameCanonical($this->faker->lastName);
        $user2->setPlainPassword($testPassword);
        $user2->setEmail("admin2@example.com");
        $user2->setRoles(['ROLE_ADMIN']);
        $user2->setEnabled(true);
        $user2->setExtraFields($this->getReference('admin-two'));
        $this->addReference('admin2', $user2);
        $this->userManager->updateUser($user2);

        $user3 = new User();
        $user3->setUsername($this->faker->firstName);
        $user3->setUsernameCanonical($this->faker->lastName);
        $user3->setPlainPassword($testPassword);
        $user3->setEmail("user1@example.com");
        $user3->setRoles(['ROLE_USER']);
        $user3->setEnabled(true);
        $user3->setExtraFields($this->getReference('user-one'));
        $this->addReference('user1', $user3);
        $this->userManager->updateUser($user3);

        $user4 = new User();
        $user4->setUsername($this->faker->firstName);
        $user4->setUsernameCanonical($this->faker->lastName);
        $user4->setPlainPassword($testPassword);
        $user4->setEmail("user2@example.com");
        $user4->setRoles(['ROLE_USER']);
        $user4->setEnabled(true);
        $user4->setExtraFields($this->getReference('user-two'));
        $this->addReference('user2', $user4);
        $this->userManager->updateUser($user4);
    }

    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}