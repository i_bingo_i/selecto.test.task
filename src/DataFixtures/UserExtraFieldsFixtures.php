<?php

namespace App\DataFixtures;

use App\Entity\UserExtraFields;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserExtraFieldsFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $admin1ExtraFields = new UserExtraFields();
        $admin1ExtraFields->setPhone('+380979779797');
        $admin1ExtraFields->setSurname('AdminOne');

        $manager->persist($admin1ExtraFields);
        $this->addReference('admin-one', $admin1ExtraFields);


        $admin2ExtraFields = new UserExtraFields();
        $admin2ExtraFields->setPhone('+380976767676');
        $admin2ExtraFields->setSurname('AdminTwo');

        $manager->persist($admin2ExtraFields);
        $this->addReference('admin-two', $admin2ExtraFields);

        $user1ExtraFields = new UserExtraFields();
        $user1ExtraFields->setPhone('+380976666666');
        $user1ExtraFields->setSurname('UserOne');

        $manager->persist($user1ExtraFields);
        $this->addReference('user-one', $user1ExtraFields);

        $user2ExtraFields = new UserExtraFields();
        $user2ExtraFields->setPhone('+380975555555');
        $user2ExtraFields->setSurname('UserTwo');

        $manager->persist($user1ExtraFields);
        $this->addReference('user-two', $user2ExtraFields);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}