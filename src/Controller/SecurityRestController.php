<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as Rest;

class SecurityRestController
{
    /**
     * Login user
     *
     * @SWG\Post(
     *    consumes={"application/x-www-form-urlencoded"},
     *    produces={"application/json"},
     *    path="/api/login",
     *    tags={"Security"},
     *    @SWG\Parameter(
     *        in="formData",
     *        name="username",
     *        required=true,
     *        type="string",
     *        minimum=4,
     *        maximum=64
     *    ),
     *    @SWG\Parameter(
     *        in="formData",
     *        name="password",
     *        required=true,
     *        type="string",
     *        minimum=6,
     *        maximum=64
     *    ),
     *    @SWG\Response(
     *        response=200,
     *        description="Login user",
     *        @SWG\Schema(
     *            type="object",
     *            @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *        )
     *    )
     * )
     * @Rest\View(serializerGroups={"full"})
     * @Route("/api/login", methods={"POST"})
     * @param Request $request
     */
    public function login(Request $request)
    {
    }
}