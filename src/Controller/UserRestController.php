<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Services\User\GetterService;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;


class UserRestController extends FOSRestController
{
    /**
     * Find user by role
     *
     * @SWG\Tag(name="Users")
     * @Route("/api/user", methods={"GET"})
     * @Security(name="Bearer")
     * @SWG\Parameter(
     *        in="query",
     *        name="role",
     *        required=true,
     *        type="string",
     *        minimum=6,
     *        maximum=64,
     *        description="Role like 'admin' or 'user'"
     *    ),
     * @SWG\Response(
     *     response=200,
     *     description="Find user by ID",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     * @Rest\View(serializerGroups={"full"}, serializerEnableMaxDepthChecks=true)
     * @param Request $request
     * @return View
     */
    public function getUserByRole(Request $request)
    {
        /** @var GetterService $userGetterService */
        $userGetterService = $this->get(GetterService::class);
        /** @var User $usr */
        $usr= $this->get('security.token_storage')->getToken()->getUser();

        $userGetterService->getUserByRole($request->get('role'), $user);

        return new View($user, Response::HTTP_OK);
    }
}