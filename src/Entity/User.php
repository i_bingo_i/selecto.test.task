<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /** @var string */
    protected $username;
    /** @var string */
    protected $email;
    /** @var array */
    protected $roles;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserExtraFields", inversedBy="user")
     * @JoinColumn(name="extra_fields_id", referencedColumnName="id")
     */
    protected $extraFields;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @return UserExtraFields
     */
    public function getExtraFields()
    {
        return $this->extraFields;
    }

    /**
     * @param UserExtraFields $extraFields
     */
    public function setExtraFields(UserExtraFields $extraFields): void
    {
        $this->extraFields = $extraFields;
    }

}