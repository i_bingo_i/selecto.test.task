<?php

namespace App\Services\User;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;

class GetterService
{
    /** @var UserRepository  */
    private $userRepository;

    /**
     * GetterService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->userRepository = $entityManager->getRepository('App:User');
    }

    public function getUserByRole($role)
    {
        $this->userRepository->findByRole($role);
    }
}