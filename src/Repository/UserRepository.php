<?php
namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository
{
    /**
     * UserRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param $role
     * @return mixed
     */
    public function findByRole($role)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->where('u.roles LIKE :roles')
            ->setParameter('roles', '%'.$role.'%');

        return $qb->getQuery()->getResult();
    }
}