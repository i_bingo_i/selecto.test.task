<?php

namespace App\Repository;

use App\Entity\UserExtraFields;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserExtraFields|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserExtraFields|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserExtraFields[]    findAll()
 * @method UserExtraFields[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserExtraFieldsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserExtraFields::class);
    }

//    /**
//     * @return UserExtraFields[] Returns an array of UserExtraFields objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserExtraFields
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
